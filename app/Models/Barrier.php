<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barrier extends Model
{
    protected $table = 'barriers';

        public function sheeps()
        {
            return $this->hasMany('App\Models\Sheep','barrier_id','id');
        }

}
