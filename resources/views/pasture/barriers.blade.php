@extends('layouts.main')
@section('content')
    <p id="days" style="text-align:center;font-size:30px "></p>
    <p style="text-align: center"><a href="{{url('/report')}}" >Просмотреть отчёт</a></p>
    <div class="row" id="app">
        <p class="col-md-12" style="text-align: center">
            <span style="color:#4dc0b5; font-size: 30px">Зеленые овцы только родились или недавно заселились, ещё не готовы к резне</span><br>
            <button @click="refreshSheep()" type="button" class="btn btn-warning">Нажмите что бы можно было резать новых овец</button>
        </p>


        @foreach($barriers as $barrier)
            <div class="col-md-6">
                <div class="card">
                    <div style="background: #1b1e21" class="card-header">
                        <span style="color:wheat">{{ $barrier->name  }}</span>
                    </div>
                    <div class="card-body">
                        <blockquote class="blockquote mb-0">
                            <div id="{{$barrier->id}}" class="row">
                                @foreach($barrier->sheeps as $sheep)
                                    @if($sheep->state === 1)
                                    <div class="col-md-2" id="{{$sheep->id}}" style="margin-bottom: 15px">
                                        <span style="width: 20px">{{$sheep->name}}</span>
                                        <a href="#" @click="removeSheep({{$sheep}})" data-type="{{$sheep->id}}"><img
                                                    src="{{URL::asset('/img/kisspng-sheep-cattle-goat-livestock-icon-sheep-5a976ce6c7d518.6587239715198732548185.jpg')}}"></a>
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                        </blockquote>

                    </div>
                </div>

            </div>
        @endforeach
    </div>

@endsection

@section('script')
    <script>
        let count = 0;
        let counter = 0;
        setInterval(birthSheep, 10000);
        var example2 = new Vue({
            el: '#app',
            data: {},
            // определяйте методы в объекте `methods`
            methods: {
                removeSheep: function (sheep) {
                    swal({
                        title: "Хотите зарезать овечку?",
                        text: "Если Вы зарубите овечку, она никогда больше не сможет бегать по полям",
                        icon: "warning",
                        buttons: {
                            cancel: "Отмена",
                            confirm: {
                                text: "Сохранить",
                                value: "catch",
                            },
                        },
                        dangerMode: false,
                    }).then((value => {
                        if (value === 'catch') {
                            if (sheep) {
                                axios.delete('/slaughter/'+sheep.id+'/'+localStorage.getItem('day'), {}).then(function (response) {
                                    if (response.data === 'success') {
                                        $('#' + sheep.id).remove();
                                        swal({
                                            title: 'Готово',
                                            icon: "success",
                                            timer:1000,
                                        });
                                        return true;
                                    } else if(response.data['error'] === 'error'){
                                        swal({
                                            title: 'Невозможно удалить последнюю овечку из загона '+response.data['barrier'],
                                            icon: "warning",
                                        });
                                    }
                                })
                                    .catch(function (error) {
                                        console.log(error)
                                    });
                            }
                        }
                    }));
                    return false;
                },
                refreshSheep:function () {
                    location.reload()
                }
            }
        });
        function birthSheep() {
            days();
            axios.get('/birth/'+localStorage.getItem('day'), {}).then(function (response) {
                if (response.data['success'] === 'success') {

                    $('#days').html('День ' + localStorage.getItem('day'));

                    swal({
                        title: 'В загоне ' + response.data['barrier'] + ' родилась овечка ' + response.data['sheep_id'].name,
                        icon: "success",
                        timer:2000,

                    }).then(
                            $('#' + response.data['barrier_id']).append($(`<div id="${response.data['sheep_id'].id}"  class="col-md-2" style="margin-bottom: 15px">
                                        <span>${response.data['sheep_id'].name}</span>
                                        <a href="#" data-type="${response.data['sheep_id'].id}"><img src="{{URL::asset('/img/green.jpg')}}" ></a>
                                    </div>`))
                    )
                } else if (response.data['moved'] === 'moved'){
                    swal({
                        title: 'Пересажена овечка в загон ' + response.data['barrier'] + ' с именем ' + response.data['sheep_id'].name,
                        icon: "success",
                        timer:2000,

                    }).then(
                        $('#' + response.data['barrier_id']).append($(`<div id="${response.data['sheep_id'].id}" style="margin-bottom: 15px">
                                        <span>${response.data['sheep_id'].name}</span>
                                        <a href="#" data-type="${response.data['sheep_id'].id}"><img src="{{URL::asset('/img/green.jpg')}}" ></a>
                                    </div>`))
                    )
                }
            });
        }
        function days() {
            counter = localStorage.getItem('MassacreDay');

            if(Number(counter) >= 10){

                axios.delete('/tenthDay/'+localStorage.getItem('day'), {}).then(function (response) {
                    if (response.data['success'] === 'success') {
                        $('#' + response.data['sheep'].id).remove();
                        swal({
                            title: 'Зарезали овечку ' +response.data['sheep'].name+ ' на 10 день',
                            icon: "success",
                            timer:2000
                        });
                        return true;
                    }else if(response.data['error'] === 'error') {

                        swal({
                            title: 'Невозможно зарезать овечку так как осталась одна овечка',
                            icon: "success",
                        });
                        return false;
                    }
                })
                    .catch(function (error) {
                        console.log(error)
                    });
                counter = 0;
                localStorage.removeItem('MassacreDay')
            }

            let MassacreDay = Number(counter) + 1;

            localStorage.setItem('MassacreDay',MassacreDay);

            count = localStorage.getItem('day');

            if(localStorage.getItem('day') == null){
                count = 0
            }

            let day = Number(count) + 1;
            localStorage.setItem('day', day);
        }

        $('#days').html('День ' + localStorage.getItem('day'));

    </script>
@endsection


