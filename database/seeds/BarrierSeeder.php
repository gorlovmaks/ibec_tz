<?php

use Illuminate\Database\Seeder;

class BarrierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $barrier1 = new \App\Models\Barrier();
        $barrier1->name = 'Группа-1';
        $barrier1->save();

        $barrier2 = new \App\Models\Barrier();
        $barrier2->name = 'Группа-2';
        $barrier2->save();

        $barrier3 = new \App\Models\Barrier();
        $barrier3->name = 'Группа-3';
        $barrier3->save();

        $barrier4 = new \App\Models\Barrier();
        $barrier4->name = 'Группа-4';
        $barrier4->save();
    }
}
