<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/'], function() {
    Route::get('/', ['as' => 'get_sheeps', 'uses' => 'SheepsController@getBarriers']);
    Route::get('/report', ['as' => 'report', 'uses' => 'SheepsController@reportByDays']);
    Route::delete('/slaughter/{id}/{day}/', ['as' => 'slaughter-sheep', 'uses' => 'SheepsController@slaughterSheep']);
    Route::get('/birth/{day}', ['as' => 'birth-sheep', 'uses' => 'SheepsController@birthSheep']);
    Route::delete('/tenthDay/{day}', ['as' => 'tenth-day-sheep', 'uses' => 'SheepsController@removeOnTenthDay']);
});

