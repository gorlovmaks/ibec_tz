<?php

use Illuminate\Database\Seeder;

class SheepsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $names_sheep = [
            'Cерега',
            'Генадий',
            'Акылбек',
            'Виктория',
            'Зарина',
            'Данила',
            'Мунька',
            'Давидыч',
            'Гитлер',
            'Даниелла',
            'Настенька',
        ];

        $barriers = \App\Models\Barrier::all();
        foreach ($barriers as $barrier){
            for($i = 0; $i < 3; $i++){
                $item1 = new \App\Models\Sheep();
                $item1->name = $names_sheep[$i];
                $item1->date_of_birth = 1;
                $item1->state = 1;
                $item1->barrier_id = $barrier->id;
                $item1->save();
            }
        }

    }
}
