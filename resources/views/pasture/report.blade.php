@extends('layouts.main')
@section('content')
    <div class="container">
        <div style="text-align: center;margin: 0 auto">
            <h1>Страница для запроса отчёта по дням</h1>
            <p id="days" style="font-size:30px "></p>

            <div style="width: 300px; margin: 0 auto">
                <form method="get" action="{{url('/report')}}">
                    <div class="form-group">
                        <label for="from">От</label>
                        <input type="text" name="day_from" class="form-control" id="from" aria-describedby="emailHelp"
                               placeholder="От 1">

                    </div>
                    <div class="form-group">
                        <label for="before">До</label>
                        <input type="text" name="day_before" class="form-control" id="before" placeholder="">
                    </div>
                    <small id="emailHelp" class="form-text text-muted">Надеемся вы введёте правильные данные</small>
                    <br>
                    <button type="submit" class="btn btn-primary">Запросить</button>
                </form>
            </div>
        </div>
        <div class="row">
        <div class="col-md-6" style="margin:0 auto;">
            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Живых</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">Итого</th>
                    <th scope="col">{{$sumLives}}</th>
                </tr>
                @foreach($liveSheeps as $sheep)
                    <tr>
                        <td class="table-active">День - {{$sheep->date_of_birth}} </td>
                        <td class="bg-success">В загоне c ID - {{$sheep->barrier_id}} , родилась овечка {{$sheep->name}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

        <div class="col-md-6" style="margin:0 auto;">
            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Убитых</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">Итого</th>
                    <th scope="col">{{$sumDeath}}</th>
                </tr>
                @foreach ($deadSheeps as $sheep)
                    <tr>
                        <td class="table-active">День - {{$sheep->date_of_death}} </td>
                        <td class="bg-info">В загоне c ID - {{$sheep->barrier_id}} , зарезали овечку {{$sheep->name}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        </div>
    </div>

    <script>
        $('#days').html('День ' + localStorage.getItem('day'))
        $("#before").attr("placeholder", 'До ' + localStorage.getItem('day'))

    </script>
@endsection


