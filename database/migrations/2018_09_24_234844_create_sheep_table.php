<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSheepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sheeps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('date_of_birth');
            $table->integer('date_of_death')->nullable();
            $table->boolean('state');
            $table->integer('barrier_id')->unsigned()->nullable();
            $table->foreign('barrier_id')->references('id')->on('barriers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
