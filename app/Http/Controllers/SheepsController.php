<?php

namespace App\Http\Controllers;

use App\Models\Barrier;
use App\Models\Sheep;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SheepsController extends Controller
{

    // Метод вывода всех барьеров и всех овечек на главной странице
    public function getBarriers()
    {
        $barriers = Barrier::all();

        foreach ($barriers as $barrier) {
            $sheeps[] = $barrier->sheeps;
        }

        return view('pasture.barriers')->with(compact('sheeps', 'barriers'));

    }

    // Метод убийства овечки на мясо при нажатии на неё
    public function slaughterSheep($id, $day)
    {
        $barrier = Sheep::findOrFail($id)->barrier;

        $liveSheeps = [];

        foreach ($barrier->sheeps as $sheep) {
            if ($sheep->state == 1) {
                $liveSheeps [] = $sheep;
            }
        }

        if (count($liveSheeps) > 1) {

            $sheep = Sheep::findOrFail($id);
            $sheep->date_of_death = $day;
            $sheep->state = false;
            $sheep->save();
            $result = 'success';

        } else if (count($liveSheeps) <= 1) {
            $result = ['error' => 'error', 'barrier' => $barrier->name];
        }

        return $result;
    }

    // Метод рандомного рождения овечки
    public function birthSheep($day)
    {
         $names_sheep = [
            'Cерега',
            'Генадий',
            'Акылбек',
            'Виктория',
            'Зарина',
            'Данила',
            'Мунька',
            'Давидыч',
            'Гитлер',
            'Даниелла',
            'Настенька',
        ];

        $barriers = Barrier::all();
        $liveSheeps = [];
        $barrier = $barriers[rand(0, count($barriers) - 1)];

        foreach ($barrier->sheeps as $sheep){
            if($sheep->state == 1){
                $liveSheeps [] = $sheep;
            }
        }

        if (count($liveSheeps) > 1) {
            $sheep = new Sheep();
            $sheep->name = $names_sheep[rand(0, count($names_sheep) - 1)];
            $sheep->barrier_id = $barrier->id;
            $sheep->date_of_birth = $day;
            $sheep->state = 1;
            $sheep->save();
            $result = [
                'success' => 'success',
                'barrier' => $barrier->name,
                'sheep_id' => $sheep,
                'barrier_id' => $barrier->id
            ];
        }else if (count($liveSheeps) <= 1){
            $sheep = $this->MovingSheep($barrier->id);
            $result = [
                'moved' => 'moved',
                'barrier' => $barrier->name,
                'sheep_id' => $sheep,
                'barrier_id' => $barrier->id
            ];
        }
        return $result;
    }


    // Метод убийства овечки на мясо на каждый десятый день
    public function removeOnTenthDay($day)
    {
        $barriers = Barrier::all();
        $randomBarrierSheeps = $barriers[rand(0, count($barriers) - 1)];
        $sheeps = $randomBarrierSheeps->sheeps;

        $liveSheeps = [];

        foreach ($sheeps as $valueSheep) {
            if ($valueSheep->state == 1) {
                $liveSheeps [] = $valueSheep;
            }
        }
        $sheep = $liveSheeps[rand(0, count($liveSheeps) - 1)];
        if (count($liveSheeps) > 1) {
            if ($sheep) {
                $sheep = Sheep::findOrFail($sheep->id);
                $sheep->date_of_death = $day;
                $sheep->state = false;
                $sheep->save();

                $result = ['success' => 'success', 'sheep' => $sheep];
            }
        } else if (count($liveSheeps) <= 1) {
            $result = 'error';
        }

        return $result;
    }


    // Метод отчёта по дням
    public function reportByDays(Request $request)
    {
        $total = 0;

        $day_from = $request->input('day_from');
        $day_before = $request->input('day_before');
        $liveSheeps = [];
        $deadSheeps = [];
        $sumLives = 0;
        $sumDeath = 0;
        if($request->input('day_from')){


            $liveSheeps = DB::table('sheeps')->select('*')
                ->whereBetween('date_of_birth', [$day_from, $day_before])->orderBy('date_of_birth')->get();

            $deadSheeps = DB::table('sheeps')->select('*')
                ->whereBetween('date_of_death', [$day_from, $day_before])->orderBy('date_of_death')->get();

            $total = $liveSheeps->count() + $deadSheeps->count();

            $sumLives = $liveSheeps->count();
            $sumDeath = $deadSheeps->count();
        }

        return view('pasture.report')->with(compact(
            'liveSheeps',
            'deadSheeps',
            'total',
            'sumDeath',
            'sumLives'
        ));
    }

    public function MovingSheep($id){
        $count = 0;
        //Ищем сумму всех овечек в из каждого загона
        $sum_sheeps_barriers = DB::table('barriers')
            ->select('barriers.id',DB::raw('count(*) as `amount_sheeps`'))
            ->leftJoin('sheeps', 'barriers.id', '=', 'sheeps.barrier_id')
            ->where('sheeps.state','=',1)
            ->groupBy('sheeps.barrier_id')
            ->orderBy('amount_sheeps')
            ->get();

        //Находим самый большой загон
        foreach ($sum_sheeps_barriers as $barrier){
            if($barrier->amount_sheeps > $count){
                $count = $barrier->amount_sheeps;
                $maxBarrier_id = $barrier->id;
            }
        }

        $maxBarrier = Barrier::findOrFail($maxBarrier_id);
        $randomSheep = $maxBarrier->sheeps[rand(0,count($maxBarrier->sheeps) - 1)];
        $movedSheep = Sheep::findOrFail($randomSheep->id);
        $movedSheep->barrier_id = $id;
        $movedSheep->save();
        return $movedSheep;
    }
}


