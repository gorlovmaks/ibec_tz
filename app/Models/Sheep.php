<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sheep extends Model
{
    protected $table = 'sheeps';

    public function barrier()
    {
        return $this->belongsTo('App\Models\Barrier','barrier_id','id');
    }
}
